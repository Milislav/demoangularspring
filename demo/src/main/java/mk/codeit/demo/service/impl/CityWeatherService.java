package mk.codeit.demo.service.impl;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import mk.codeit.demo.model.CityWeather;
import mk.codeit.demo.model.CurrentWeather;
import mk.codeit.demo.repository.CityWeatherRepository;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriTemplate;

import javax.sound.midi.Soundbank;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.net.URI;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CityWeatherService {

    private static final String Weather_Url = "http://api.weatherapi.com/v1/history.json?key={key}&q={city}&dt={date}";;
    String apiKey = "82663a50264e47fc922114901210806";

    private final CityWeatherRepository cityWeatherRepository;
    private final RestTemplate restTemplate;
    private final ObjectMapper objectMapper;

    public CityWeatherService(CityWeatherRepository cityWeatherRepository, RestTemplateBuilder restTemplateBuilder, ObjectMapper objectMapper) {
        this.cityWeatherRepository = cityWeatherRepository;
        this.restTemplate = restTemplateBuilder.build();
        this.objectMapper = objectMapper;
    }

    public CityWeather getWeather(String city, String date)  {


        UriTemplate url = new UriTemplate(Weather_Url);
        URI URLE = url.expand(apiKey,city,date);

      //System.out.println(URLE + " = URLE");
        ResponseEntity<String> response = restTemplate.getForEntity(URLE, String.class);

        return convert(response);
    }

    @Transactional
    CityWeather convert(ResponseEntity<String> response) {
        try {
            JsonNode root = objectMapper.readTree(response.getBody());


            CityWeather cityWeather = new CityWeather(
                    root.path("location").path("name").asText(),
                    root.path("forecast").path("forecastday").get(0).path("date").asText(),
                    root.path("forecast").path("forecastday").get(0).path("day").path("condition").path("text").asText(),
                    root.path("forecast").path("forecastday").get(0).path("day").path("maxtemp_c").asDouble());

            /*
            System.out.println(root.path("location").path("name").asText());
            System.out.println(root.path("forecast").path("forecastday").get(0).path("date").asText());
            System.out.println(root.path("forecast").path("forecastday").get(0).path("day").path("condition").path("text").asText());
            System.out.println(root.path("forecast").path("forecastday").get(0).path("day").path("maxtemp_c").asText());
            */

            if(this.cityWeatherRepository.findByCityAndAndDate(cityWeather.getCity(), cityWeather.getDate()).isPresent())
               this.cityWeatherRepository.deleteById(this.cityWeatherRepository.findByCityAndAndDate(cityWeather.getCity(), cityWeather.getDate()).get().getId());

                this.cityWeatherRepository.saveAndFlush(cityWeather);


            return cityWeather;
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Error parsing JSON", e);
        }
    }

    public List<CityWeather> listAll(){
        return this.cityWeatherRepository.findAll();
    }


    public void PullData(int num){

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime now = LocalDateTime.now();

        for(int i = 0 ; i <= num ; i++) {
            getWeather("Skopje", dtf.format(now.minusDays(i)));
            getWeather("Bitola", dtf.format(now.minusDays(i)));
            getWeather("Ohrid", dtf.format(now.minusDays(i)));
        }



    }


    public List<CityWeather> above25(){

        return this.cityWeatherRepository.findAll().
                stream().filter(cityWeather -> cityWeather.getMax_temperature() > 25 ).collect(Collectors.toList());
    }

}
