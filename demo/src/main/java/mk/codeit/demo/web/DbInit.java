package mk.codeit.demo.web;

import mk.codeit.demo.service.impl.CityWeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class DbInit {


    @Autowired
    CityWeatherService cityWeatherService;

    @PostConstruct
    private void postConstruct() {
        cityWeatherService.PullData(7);
    }
}
