package mk.codeit.demo.web.restcontroller;

import mk.codeit.demo.model.CurrentWeather;
import mk.codeit.demo.service.LiveWeatherService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/weather-cities")
public class CurrentWeatherRestController {

    private final LiveWeatherService liveWeatherService;

    public CurrentWeatherRestController(LiveWeatherService liveWeatherService) {
        this.liveWeatherService = liveWeatherService;
    }

    @GetMapping
    List<CurrentWeather> getWeateher(){
       // liveWeatherService.getCurrentWeather("Skopje","Mk");

        return liveWeatherService.listWeather();
    }
}
