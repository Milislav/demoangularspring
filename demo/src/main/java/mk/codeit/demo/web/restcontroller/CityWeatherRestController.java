package mk.codeit.demo.web.restcontroller;

import mk.codeit.demo.model.CityWeather;
import mk.codeit.demo.service.impl.CityWeatherService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("api/city-weather-date")
@CrossOrigin(origins = "http://localhost:4200")
public class CityWeatherRestController {

    private final CityWeatherService cityWeatherService;

    public CityWeatherRestController(CityWeatherService cityWeatherService) {
        this.cityWeatherService = cityWeatherService;
    }

    @GetMapping
    public List<CityWeather> getCities(){


        return cityWeatherService.listAll();
    }

    @GetMapping("/above-25")
    public List<CityWeather> getCitiesAbove25(){
       // cityWeatherService.PullData(7);

        return cityWeatherService.above25();
    }


}
