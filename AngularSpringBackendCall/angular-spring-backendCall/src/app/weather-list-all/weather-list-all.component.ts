import { Component, OnInit } from '@angular/core';
import {WeatherService} from "../shared/weather.service";
import {WeatherCity} from "../model/weather-city";

@Component({
  selector: 'app-weather-list-all',
  templateUrl: './weather-list-all.component.html',
  styleUrls: ['./weather-list-all.component.css']
})
export class WeatherListAllComponent implements OnInit {

  weatherAll:WeatherCity[];
  constructor(private weatherService:WeatherService) {
    this.weatherAll = [];
  }

  ngOnInit(): void {
    this.weatherService.getAll().subscribe(value => { //2 zagradi nz zasto
      this.weatherAll = value;
    });
  }

}
