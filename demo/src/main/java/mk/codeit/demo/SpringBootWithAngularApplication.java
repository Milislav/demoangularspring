package mk.codeit.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.ComponentScan;

import javax.annotation.PostConstruct;

@SpringBootApplication
@ServletComponentScan
public class SpringBootWithAngularApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootWithAngularApplication.class, args);

    }

}
