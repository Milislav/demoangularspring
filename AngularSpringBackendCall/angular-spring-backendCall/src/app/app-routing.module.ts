import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {WeatherListComponent} from "./weather-list/weather-list.component";
import {WeatherListAllComponent} from "./weather-list-all/weather-list-all.component";


const routes: Routes = [
  { path: 'weather' , component:WeatherListComponent },
  {path:'weatherAll' , component:WeatherListAllComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
