export class WeatherCity {
  id :string;
  city :string;
  date : string;
  description : string;
  max_temperature : number;


  constructor(id: string, city: string, date: string, description: string, max_temperature: number) {
    this.id = id;
    this.city = city;
    this.date = date;
    this.description = description;
    this.max_temperature = max_temperature;
  }
}
