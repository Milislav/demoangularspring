import {Injectable, OnInit} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {WeatherCity} from "../model/weather-city";

@Injectable({
  providedIn: 'root'
})
export  class WeatherService {

  private weatherAbove25Url: string;
  private weatherAllurl: string;


  constructor(private http: HttpClient) {
    this.weatherAbove25Url = 'http://localhost:8081/api/city-weather-date/above-25';
    this.weatherAllurl = 'http://localhost:8081/api/city-weather-date'
  }

  getAll():Observable<WeatherCity[]>{

    return this.http.get<WeatherCity[]>(this.weatherAllurl);

  }

  getAbove25():Observable<WeatherCity[]>{

    return this.http.get<WeatherCity[]>(this.weatherAbove25Url);

  }




}
