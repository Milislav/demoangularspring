package mk.codeit.demo.service;

import mk.codeit.demo.model.CurrentWeather;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class StubWeatherService {

    public CurrentWeather getCurrentWeather(String city, String country) {
        return new CurrentWeather("Mk", "Sk" ,"Clear", BigDecimal.ONE, BigDecimal.ZERO, BigDecimal.TEN);
    }
}
