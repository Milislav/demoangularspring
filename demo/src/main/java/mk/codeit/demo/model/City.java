package mk.codeit.demo.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class City {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long Id;

    String name;

    @ManyToOne
    Weather weather;

    public City() {
    }

    public City(Long id, String name, Weather weather) {
        Id = id;
        this.name = name;
        this.weather = weather;
    }
}
