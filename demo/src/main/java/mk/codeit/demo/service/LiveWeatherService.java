package mk.codeit.demo.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import mk.codeit.demo.model.CurrentWeather;
import mk.codeit.demo.repository.CurrentWeatherRepository;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriTemplate;

import java.math.BigDecimal;
import java.net.URI;
import java.util.List;


@Service
public class LiveWeatherService {

    private static final String WEATHER_URL = "http://api.openweathermap.org/data/2.5/weather?q={city},{country}&APPID={key}&units=metric";;


   // @Value("${api.openweathermap.key}")
    private final String apiKey = "66faac6c92af643b1a6c9b06ff7d76a8";

    private final RestTemplate restTemplate;
    private final ObjectMapper objectMapper;
    private final CurrentWeatherRepository currentWeatherRepository;

    public LiveWeatherService(RestTemplateBuilder restTemplateBuilder, ObjectMapper objectMapper, CurrentWeatherRepository currentWeatherRepository) {
        this.restTemplate = restTemplateBuilder.build();
        this.objectMapper = objectMapper;

        this.currentWeatherRepository = currentWeatherRepository;
    }


    public CurrentWeather getCurrentWeather(String city, String country)  {


        UriTemplate url = new UriTemplate(WEATHER_URL);
        URI URLE = url.expand(city,country,apiKey);


        ResponseEntity<String> response = restTemplate.getForEntity(URLE, String.class);

        return convert(response);
    }

    private CurrentWeather convert(ResponseEntity<String> response) {
        try {
            JsonNode root = objectMapper.readTree(response.getBody());


            CurrentWeather cw = new CurrentWeather( root.path("sys").path("country").asText(),
                    root.path("name").asText(),
                    root.path("weather").get(0).path("main").asText(),
                    BigDecimal.valueOf(root.path("main").path("temp").asDouble()),
                    BigDecimal.valueOf(root.path("main").path("feels_like").asDouble()),
                    BigDecimal.valueOf(root.path("wind").path("speed").asDouble()));

            currentWeatherRepository.saveAndFlush(cw);

            return cw;

        } catch (JsonProcessingException e) {
            throw new RuntimeException("Error parsing JSON", e);
        }
    }

    public List<CurrentWeather> listWeather(){
        return this.currentWeatherRepository.findAll();
    }
}