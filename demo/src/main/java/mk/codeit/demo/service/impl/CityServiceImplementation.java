package mk.codeit.demo.service.impl;

import lombok.SneakyThrows;
import mk.codeit.demo.model.City;
import mk.codeit.demo.repository.CityRepository;
import mk.codeit.demo.service.CityService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CityServiceImplementation implements CityService {


    private final CityRepository cityRepository;

    public CityServiceImplementation(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    @SneakyThrows
    @Override
    public List<City> listCities() {
        if(cityRepository.findAll().isEmpty())
            throw new Exception("There are no cities");
        return cityRepository.findAll();
    }
}
