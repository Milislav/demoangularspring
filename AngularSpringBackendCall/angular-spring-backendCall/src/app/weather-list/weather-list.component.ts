import { Component, OnInit } from '@angular/core';
import {WeatherService} from "../shared/weather.service";
import {WeatherCity} from "../model/weather-city";


@Component({
  selector: 'app-weather-list',
  templateUrl: './weather-list.component.html',
  styleUrls: ['./weather-list.component.css']
})
export class WeatherListComponent implements OnInit {

  weathersAbove25: WeatherCity[];



  constructor(private weatherService: WeatherService ) {

      this.weathersAbove25 = [];
  }

  ngOnInit() {



    this.weatherService.getAbove25().subscribe((data1 => {
      this.weathersAbove25 = data1;
    }));



  }
}
