package mk.codeit.demo.repository;

import mk.codeit.demo.model.CityWeather;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.Optional;

@Repository
@CrossOrigin(origins = "http://localhost:4200/")
public interface CityWeatherRepository extends JpaRepository<CityWeather,Long> {

    Optional<CityWeather> findByCityAndAndDate(String city,String date);
    void deleteByCityAndDate(String city,String date);

}
