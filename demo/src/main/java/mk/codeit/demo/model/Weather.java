package mk.codeit.demo.model;


import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
public class Weather {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long Id;

    String forecast;

    int temp;

    public Weather() {
    }

    public Weather(Long id, String forecast, int temp) {
        Id = id;
        this.forecast = forecast;
        this.temp = temp;
    }
}
