package mk.codeit.demo.web;

import mk.codeit.demo.model.CurrentWeather;
import mk.codeit.demo.service.LiveWeatherService;
import mk.codeit.demo.service.StubWeatherService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.math.BigDecimal;

@Controller
public class CurrentWeatherController {

    private final LiveWeatherService liveWeatherService;
    private final StubWeatherService stubWeatherService;

    public CurrentWeatherController(LiveWeatherService liveWeatherService, StubWeatherService stubWeatherService) {
        this.liveWeatherService = liveWeatherService;
        this.stubWeatherService = stubWeatherService;
    }


    @GetMapping("/current-weather")
    public String getCurrentWeather(Model model) {
        if (true) {
            model.addAttribute("currentWeather", liveWeatherService.getCurrentWeather("Skopje", "mk"));
        } else {
            model.addAttribute("currentWeather", stubWeatherService.getCurrentWeather("Detroit", "us"));
        }
        return "CurrentWeather";
    }
}