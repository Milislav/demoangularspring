import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {WeatherService} from "./shared/weather.service";
import {HttpClientModule} from "@angular/common/http";
import { WeatherListComponent } from './weather-list/weather-list.component';
import { WeatherListAllComponent } from './weather-list-all/weather-list-all.component';


@NgModule({
  declarations: [
    AppComponent,
    WeatherListComponent,
    WeatherListAllComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [WeatherService],
  bootstrap: [AppComponent]
})
export class AppModule { }
