package mk.codeit.demo.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Entity
public class CurrentWeather implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    private String city;
    private String country;
    private String description;
    private BigDecimal temperature;
    private BigDecimal feelsLike;
    private BigDecimal windSpeed;

// boilerplate getters, constructors, equals, and hashcode omitted


    public CurrentWeather() {
    }

    public CurrentWeather(String country, String city, String description, BigDecimal temperature, BigDecimal feelsLike, BigDecimal windSpeed) {
        this.country = country;
        this.city = city;
        this.description = description;
        this.temperature = temperature;
        this.feelsLike = feelsLike;
        this.windSpeed = windSpeed;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getTemperature() {
        return temperature;
    }

    public BigDecimal getFeelsLike() {
        return feelsLike;
    }

    public BigDecimal getWindSpeed() {
        return windSpeed;
    }

}
