package mk.codeit.demo.service;

import mk.codeit.demo.model.City;

import java.util.List;
import java.util.Optional;

public interface CityService {

    List<City> listCities();
}
