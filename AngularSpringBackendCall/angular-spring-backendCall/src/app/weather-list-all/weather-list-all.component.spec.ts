import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WeatherListAllComponent } from './weather-list-all.component';

describe('WeatherListAllComponent', () => {
  let component: WeatherListAllComponent;
  let fixture: ComponentFixture<WeatherListAllComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WeatherListAllComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherListAllComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
