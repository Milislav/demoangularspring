package mk.codeit.demo.model;


import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
public class CityWeather {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    private String city;

    private String date;
    private String description;
    private double Max_temperature;

    public CityWeather( String city, String date, String description, double max_temperature) {

        this.city = city;
        this.date = date;
        this.description = description;
        this.Max_temperature = max_temperature;
    }

    public CityWeather() {
    }


}
